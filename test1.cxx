// SerialPacket test

#include <stdio.h>
#include <unistd.h> // write()

#include "SerialPacket.h"

void transmit(SerialPacket& p)
{
  char* buf = NULL;
  int nbytes = 0;
  p.GetPacket(1, &buf, &nbytes);
  if (nbytes > 0) {
    write(1, buf, nbytes);
  }
}

int main(int argc, char* argv[])
{
  SerialPacket p;

  p.WriteString("get version"); transmit(p);
  p.WriteString("get all"); transmit(p);

}

// end
