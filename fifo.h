// fifo.h

#define FIFO_EOF (-1)
#define FIFO_EMPTY (-2)

void fifo_esp32_init();
int  fifo_esp32_read();
void fifo_esp32_write(const char* buf, int nbytes);

void fifo_gui_init();
int  fifo_gui_read();
void fifo_gui_write(const char* buf, int nbytes);

// end
