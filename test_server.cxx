// SerialPacket test

#include <stdio.h>
#include <unistd.h> // usleep()
#include <string.h> // strcmp()

#include "SerialPacket.h"
#include "fifo.h"

void transmit(SerialPacket& p)
{
  char* buf = NULL;
  int nbytes = 0;
  p.GetPacket(1, &buf, &nbytes);
  if (nbytes > 0) {
    printf("transmit: [%s]\n", buf);
    fifo_esp32_write(buf, nbytes);
  }
}

void command(SerialPacket& p, const char* cmd)
{
  if (strcmp(cmd, "get all") == 0) {
    p.WriteString("OK - 0.0,1.1,2.2");
  } else {
    p.WriteString("ERROR - unknown command: ");
    p.AddString(cmd);
  }

  transmit(p);
}

int main(int argc, char* argv[])
{
  SerialPacket p;

  fifo_esp32_init();

  while (1) {
    int c = fifo_esp32_read();
    if (c == FIFO_EOF) {
      break;
    }
    if (c == FIFO_EMPTY) {
      usleep(1000);
      continue;
    }
    int packet_type = 0;
    char* buf = NULL;
    int nbytes = 0;
    p.Receive(c, &packet_type, &buf, &nbytes);
    if (packet_type > 0) {
      printf("Received: packet_type %d, bytes %d [%s]\n", packet_type, nbytes, buf);
      command(p, buf);
    }
  }
}

// end
