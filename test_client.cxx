// SerialPacket test

#include <stdio.h>
#include <unistd.h> // write()

#include "SerialPacket.h"
#include "fifo.h"

void wait_reply(SerialPacket& p, int timeout, int* packet_type, char** rbuf, int *rnbytes)
{
  *packet_type = 0;
  *rbuf = NULL;
  *rnbytes = 0;

  for (int i=0; i<timeout; i++) {
    int c = fifo_gui_read();
    if (c == FIFO_EOF) {
      break;
    }
    if (c == FIFO_EMPTY) {
      usleep(1000);
      continue;
    }

    p.Receive(c, packet_type, rbuf, rnbytes);
    if (*packet_type > 0) {
      return;
    }
  }
}

void transaction(SerialPacket& p, int* packet_type, char** rbuf, int *rnbytes)
{
  char* buf = NULL;
  int nbytes = 0;
  p.GetPacket(1, &buf, &nbytes);
  if (nbytes > 0) {
    printf("transmit: [%s]\n", buf);
    fifo_gui_write(buf, nbytes);
    wait_reply(p, 100, packet_type, rbuf, rnbytes);
  }
}

void test(SerialPacket& p, const char* command)
{
  printf("test: command: [%s]\n", command);
  int packet_type = 0;
  char* rbuf = NULL;
  int rnbytes = 0;
  p.WriteString(command);
  transaction(p, &packet_type, &rbuf, &rnbytes);
  if (packet_type > 0) {
    printf("test: reply: [%s] packet_type %d\n", rbuf, packet_type);
  }
}

int main(int argc, char* argv[])
{
  SerialPacket p;

  fifo_gui_init();

  test(p, "get version");
  test(p, "get all");
}

// end
