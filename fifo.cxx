// fifo.cxx

#include <stdio.h>
#include <assert.h>
#include <fcntl.h> // open()
#include <unistd.h> // read()
#include <string.h> // strlen()
#include <errno.h> // errno
#include <stdlib.h> // system()

#include "fifo.h"

static int rfd = 0;
static int wfd = 0;

void fifo_init()
{
  //system("mknod FIFO_GUI_TO_MVM p");
  //system("mknod FIFO_MVM_TO_GUI p");
  system("mkfifo FIFO_GUI_TO_MVM");
  system("mkfifo FIFO_MVM_TO_GUI");
  rfd = open("FIFO_GUI_TO_MVM", O_RDWR);
  assert(rfd > 0);
  fcntl(rfd, F_SETFL, O_NONBLOCK);
  wfd = open("FIFO_MVM_TO_GUI", O_RDWR);
  assert(wfd > 0);
  fcntl(wfd, F_SETFL, O_NONBLOCK);
}

void fifo_esp32_init()
{
  fifo_init();
}

int fifo_esp32_read()
{
  char buf[1];
  int rd = read(rfd, buf, sizeof(buf));
  if (rd < 0) {
    if (errno == EAGAIN)
      return FIFO_EMPTY;
    printf("fifo_esp32_read: read() errno %d (%s)\n", errno, strerror(errno));
    return FIFO_EOF;
  }
  return buf[0];
}

void fifo_esp32_write(const char* buf, int nbytes)
{
  int wr = write(wfd, buf, nbytes);
  assert(wr == nbytes);
}

void fifo_gui_init()
{
  fifo_init();
}

int fifo_gui_read()
{
  char buf[1];
  int rd = read(wfd, buf, sizeof(buf));
  if (rd < 0) {
    if (errno == EAGAIN)
      return FIFO_EMPTY;
    printf("fifo_gui_read: read() errno %d (%s)\n", errno, strerror(errno));
    return FIFO_EOF;
  }
  return buf[0];
}

void fifo_gui_write(const char* buf, int nbytes)
{
  int wr = write(rfd, buf, nbytes);
  assert(wr == nbytes);
}

#if 0
int fifo_read()
{
  char buf[256];
  int rd = read(rfd, buf, sizeof(buf));
  if (rd < 0) {
    if (errno == EAGAIN)
      return 0;
    printf("fifo_read: read() errno %d (%s)\n", errno, strerror(errno));
    return 0;
  }
  //printf("fifo_read: rd %d\n", rd);
  assert(rd >= 0);
  if (rd > 0) {
    buf[rd] = 0;
    //printf("fifo_read: command [%s]\n", buf);
    fifo_command(buf);
  }
  return 0;
}

int fifo_write(const char* reply)
{
  int len = strlen(reply);
  int wr = write(wfd, reply, len);
  assert(wr == len);
  return 0;
}
#endif

// end
