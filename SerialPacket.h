// SerialPacket.h

#include <stdint.h>

class SerialPacket
{
 public:
  static const int kMaxPayload = 100; // maximum packet payload size
  static const int kMaxLengthBytes = 2; // log16(kMaxPayload)
  static const int kPacketTypeBytes = 2;

 public: // sender interface
  void StartWrite();
  void WriteString(const char* string);
  void WriteBuf(const char* buf, int nbytes);
  void AddString(const char* string);
  void AddBuf(const char* buf, int nbytes);
  void GetPacket(int packet_type, char** buf, int *nbytes);

 public: // receiver interface
  void Receive(char c, int* packet_type, char** buf, int *nbytes);
  void ReceiveClear();

 private: // sender state
  int fSendPtr = 0;
  int fSendPayloadLen = 0;
  bool fSendTruncated = false;
  char fSendBuf[kMaxLengthBytes + 1 + kMaxPayload + 1 + kPacketTypeBytes + 1 + 8 + 2 + 1];

 private: // receiver state
  int fReceiveState = 0;
  int fReceiveLen = 0;
  int fReceivePtr = 0;
  int fReceivePacketType = 0;
  int fReceiveRecvCrc = 0;
  int fReceiveCrc = 0;
  char fReceiveBuf[kMaxPayload+1];

 public: // statistics
  bool fTrace = false;
  
};

// end
