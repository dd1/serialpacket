# SerialPacket

To build: run make

Test unidirection communication: ./test1.exe | ./test2.exe

Test bidirectional communication:
in one window run ./test_server.exe
in a second window run ./test_client.exe

Packet format:
```
LL:payload,TT,CCCCCCCC
where:
LL = payload length, two byte hex number, 0,1,2..255 is 00,01,02..FF.
payload = arbitrary binary or text data, max length is 255 bytes
TT = packet type, two byte little-endian hex number, 1..255 is 01..FF. Do not use packet type 0.
CCCCCCCC = 32-bit CRC32C.
```

# end
