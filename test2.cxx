// SerialPacket test

#include <stdio.h>

#include "SerialPacket.h"

int main(int argc, char* argv[])
{
  SerialPacket p;

  while (1) {
    int c = getchar();
    if (c == EOF) {
      break;
    }
    int packet_type = 0;
    char* buf = NULL;
    int nbytes = 0;
    p.Receive(c, &packet_type, &buf, &nbytes);
    if (packet_type > 0) {
      printf("Received: packet_type %d, bytes %d [%s]\n", packet_type, nbytes, buf);
    }
  }
}

// end
