// SerialPacket.cxx

#include "SerialPacket.h"
#include "crc32c.h"

#include <stdio.h>
#include <string.h> // strlen()

void SerialPacket::StartWrite()
{
  fSendTruncated = false;
  fSendPayloadLen = 0;
  fSendPtr = 0;
  for (int i=0; i<kMaxLengthBytes; i++) {
    fSendBuf[fSendPtr++] = 'X';
  }
  fSendBuf[fSendPtr++] = ':';
}

void SerialPacket::WriteString(const char* string)
{
  StartWrite();
  size_t len = strlen(string);
  AddBuf(string, len);
}

void SerialPacket::AddString(const char* string)
{
  size_t len = strlen(string);
  AddBuf(string, len);
}

void SerialPacket::WriteBuf(const char* buf, int nbytes)
{
  StartWrite();
  AddBuf(buf, nbytes);
}

void SerialPacket::AddBuf(const char* buf, int nbytes)
{
  if (fSendPayloadLen + nbytes > kMaxPayload) {
    nbytes = kMaxPayload - fSendPayloadLen;
    fSendTruncated = true;
  }
  memcpy(fSendBuf + fSendPtr, buf, nbytes);
  fSendPtr += nbytes;
  fSendPayloadLen += nbytes;
}

static char HexChar(int v)
{
  // range of v: 0..15
  if (v < 10) {
    return '0' + v;
  } else {
    return 'A' + v - 10;
  }
}

static void WriteHex2(char* ptr, int value)
{
  ptr[0] = HexChar((value&0xF0)>>4);
  ptr[1] = HexChar((value&0x0F)>>0);
}

void SerialPacket::GetPacket(int packet_type, char** buf, int* nbytes)
{
  // patch the length bytes at the start of the buffer
  WriteHex2(fSendBuf+0, fSendPayloadLen);
  // char ':' is already in the buffer
  // payload is already in the buffer
  fSendBuf[fSendPtr++] = ',';
  WriteHex2(fSendBuf+fSendPtr, packet_type);
  fSendPtr += 2;
  fSendBuf[fSendPtr++] = ',';

  uint32_t crc = 0;
  crc = crc32c(crc, fSendBuf, fSendPtr);

  fSendBuf[fSendPtr++] = HexChar((crc & 0xF0000000)>>28);
  fSendBuf[fSendPtr++] = HexChar((crc & 0x0F000000)>>24);
  fSendBuf[fSendPtr++] = HexChar((crc & 0x00F00000)>>20);
  fSendBuf[fSendPtr++] = HexChar((crc & 0x000F0000)>>16);
  fSendBuf[fSendPtr++] = HexChar((crc & 0x0000F000)>>12);
  fSendBuf[fSendPtr++] = HexChar((crc & 0x00000F00)>>8);
  fSendBuf[fSendPtr++] = HexChar((crc & 0x000000F0)>>4);
  fSendBuf[fSendPtr++] = HexChar((crc & 0x0000000F)>>0);

  fSendBuf[fSendPtr++] = '\n';
  fSendBuf[fSendPtr++] = '\r';
  fSendBuf[fSendPtr] = 0; // NUL terminate the packet string

  *buf = fSendBuf;
  *nbytes = fSendPtr;
}

#define STATE_IDLE   0
#define STATE_LEN    1
#define STATE_DATA   2
#define STATE_TYPE   3
#define STATE_CRC    4
#define STATE_ERROR  888

void SerialPacket::ReceiveClear()
{
  fReceiveLen = 0;
  fReceivePtr = 0;
  fReceiveRecvCrc = 0;
  fReceiveCrc = 0;
}

static int GetHex(char c)
{
  if (c>='0' && c<='9') {
    return c - '0';
  } else if (c>='A' && c<='F') {
    return 10 + c - 'A';
  } else {
    return -1;
  }
}

void SerialPacket::Receive(char c, int* packet_type, char** buf, int* nbytes)
{
  *packet_type = 0;
  *buf = NULL;
  *nbytes = 0;

  if (fTrace) {
    printf("state %d, char %d (%c), len %d, ptr %d, crc 0x%08X/0x%08X\n", fReceiveState, c, c, fReceiveLen, fReceivePtr, fReceiveCrc, fReceiveRecvCrc);
  }
  
  switch (fReceiveState) {
  default:
  case STATE_IDLE:
  case STATE_ERROR: {
    int v = GetHex(c);
    if (v >= 0) {
      ReceiveClear();
      fReceiveLen = v;
      fReceiveCrc = 0;
      fReceiveCrc = crc32c(fReceiveCrc, &c, 1);
      fReceiveState = STATE_LEN;
      break;
    } else if (c == '\n') {
      // eat it
    } else if (c == '\r') {
      // eat it
    } else {
      printf("STATE_ERROR: unexpected char 0x%02x, state STATE_IDLE||STATE_ERROR\n", 0xFF&c);
      fReceiveState = STATE_ERROR;
    }
    break;
  }
  case STATE_LEN: {
    if (c==':') {
      if (fReceiveLen <= kMaxPayload) {
	fReceiveCrc = crc32c(fReceiveCrc, &c, 1);
	fReceivePtr = 0;
	fReceiveState = STATE_DATA;
	break;
      } else {
	printf("STATE_ERROR: invalid payload length 0x%x bigger than maximum 0x%x bytes, state STATE_LEN after \':\'\n", fReceiveLen, kMaxPayload);
	fReceiveState = STATE_ERROR;
	break;
      }
    } else {
      int v = GetHex(c);
      if (v < 0) {
	printf("STATE_ERROR: invalid hex char 0x%02x, state STATE_LEN\n", 0xFF&c);
	fReceiveState = STATE_ERROR;
	break;
      }

      fReceiveLen = (fReceiveLen << 4) | v;

      if (fReceiveLen > kMaxPayload) {
	printf("STATE_ERROR: invalid payload length 0x%x bigger than maximum 0x%x bytes, state STATE_LEN\n", fReceiveLen, kMaxPayload);
	fReceiveState = STATE_ERROR;
	break;
      }
      
      fReceiveCrc = crc32c(fReceiveCrc, &c, 1);
    }
    break;
  }
  case STATE_DATA: {
    if (fReceivePtr < fReceiveLen) {
      fReceiveCrc = crc32c(fReceiveCrc, &c, 1);
      fReceiveBuf[fReceivePtr++] = c;
      break;
    } else if (c == ',') {
      fReceiveCrc = crc32c(fReceiveCrc, &c, 1);
      fReceivePacketType = 0;
      fReceiveState = STATE_TYPE;
      break;
    } else {
      printf("STATE_ERROR: invalid char 0x%02x, expected \',\' at the end of payload, state STATE_DATA\n", 0xFF&c);
      fReceiveState = STATE_ERROR;
      break;
    }
    break;
  }
  case STATE_TYPE: {
    if (c==',') {
      fReceiveCrc = crc32c(fReceiveCrc, &c, 1);
      fReceiveRecvCrc = 0;
      fReceivePtr = 0;
      fReceiveState = STATE_CRC;
      break;
    } else {
      int v = GetHex(c);
      if (v < 0) {
	fReceiveState = STATE_ERROR;
	break;
      }

      fReceivePacketType = (fReceivePacketType<<4) | v;
      fReceiveCrc = crc32c(fReceiveCrc, &c, 1);
    }
    break;
  }
  case STATE_CRC: {
    if (c>='0' && c<='9') {
      fReceiveRecvCrc *= 16;
      fReceiveRecvCrc += (c - '0');
      fReceivePtr++;
    } else if (c>='A' && c<='F') {
      fReceiveRecvCrc *= 16;
      fReceiveRecvCrc += 10 + (c - 'A');
      fReceivePtr++;
    } else {
      printf("STATE_ERROR: invalid hex char 0x%02x, state STATE_CRC at byte %d\n", 0xFF&c, fReceivePtr);
      fReceiveState = STATE_ERROR;
      break;
    }
    if (fReceivePtr == 8) {
      if (fReceiveCrc == fReceiveRecvCrc) {
	fReceiveBuf[fReceiveLen] = 0; // ensure strings are NUL terminated
	*packet_type = fReceivePacketType;
	*buf = fReceiveBuf;
	*nbytes = fReceiveLen;
	fReceiveState = STATE_IDLE;
	if (fTrace) {
	  printf("Receive: %d bytes, packet type %d\n", fReceiveLen, fReceivePacketType);
	}
	break;
      } else {
	printf("STATE_ERROR: CRC mismatch, computed 0x%08X, received 0x%08x, state STATE_CRC\n", fReceiveCrc, fReceiveRecvCrc);
	fReceiveState = STATE_ERROR;
	break;
      }
    }
    break;
  }
  }
}

// end
