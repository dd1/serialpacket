# Makefile

ALL+= test1.exe
ALL+= test2.exe
ALL+= test_server.exe
ALL+= test_client.exe

all:: $(ALL)

clean::
	-rm -f *.o *.exe

%.o: %.cxx
	$(CXX) -o $@ $< -c -Wall -Wuninitialized -O2 -g -std=c++11

$(ALL): %.exe: %.o SerialPacket.o crc32c.o fifo.o
	$(CXX) -o $@ $^

#end
